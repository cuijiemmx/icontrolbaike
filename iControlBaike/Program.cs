﻿using IWshRuntimeLibrary;
using System;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace iControlBaike
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AddShortcutToStartup();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Process instance = RunningInstance();
            if (instance == null)
            {
                MainForm form = new MainForm();
                try
                {
                    string[] args = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
                    if (args != null && args.Any(i => i == "/startup"))
                    {
                        form.Visible = false;
                        form.ShowInTaskbar = false;
                        form.WindowState = FormWindowState.Minimized;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace);
                }
                Application.Run(form);
            }
            else
            {
                HandleRunningInstance(instance);
            }
        }

        private static string GetShortcutPath()
        {
            var allProgramsPath = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
            var shortcutPath = Path.Combine(allProgramsPath, Properties.Resources.strPublisher);
            return Path.Combine(shortcutPath, Properties.Resources.strAppName) + ".appref-ms";
        }

        private static void AddShortcutToStartup()
        {
            if (!ApplicationDeployment.IsNetworkDeployed)
                return;
            var startupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            startupPath = Path.Combine(startupPath, Properties.Resources.strAppName) + ".lnk";
            if (System.IO.File.Exists(startupPath))
                return;
            WshShell shell = new WshShell();
            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(startupPath);
            shortcut.TargetPath = GetShortcutPath();
            shortcut.Arguments = "/startup";
            shortcut.Save();
        }

        private static Process RunningInstance()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);
            foreach (Process process in processes)
            {
                if (process.Id != current.Id)
                {
                    if (Assembly.GetExecutingAssembly().Location.Replace('/', '\\') == current.MainModule.FileName)
                    {
                        return process;
                    }
                }
            }
            return null;
        }
        private static void HandleRunningInstance(Process instance)
        {
            ShowWindowAsync(instance.MainWindowHandle, 1);
            SetForegroundWindow(instance.MainWindowHandle);
        }
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool BringWindowToTop(IntPtr hWnd);
        [DllImport("User32.dll")]
        private static extern bool ShowWindowAsync(System.IntPtr hWnd, int cmdShow);
        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(System.IntPtr hWnd);
    }
}
