// 
//  ____  _     __  __      _        _ 
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from main on 2013-08-26 00:27:05Z.
// Please visit http://code.google.com/p/dblinq2007/ for more information.
//
using System;
using System.ComponentModel;
using System.Data;
#if MONO_STRICT
	using System.Data.Linq;
#else   // MONO_STRICT
	using DbLinq.Data.Linq;
	using DbLinq.Vendor;
#endif  // MONO_STRICT
	using System.Data.Linq.Mapping;
using System.Diagnostics;


public partial class Main : DataContext
{
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		#endregion
	
	
	public Main(string connectionString) : 
			base(connectionString)
	{
		this.OnCreated();
	}
	
	public Main(string connection, MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		this.OnCreated();
	}
	
	public Main(IDbConnection connection, MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		this.OnCreated();
	}
	
	public Table<Tip> Tip
	{
		get
		{
			return this.GetTable<Tip>();
		}
	}
}

#region Start MONO_STRICT
#if MONO_STRICT

public partial class Main
{
	
	public Main(IDbConnection connection) : 
			base(connection)
	{
		this.OnCreated();
	}
}
#region End MONO_STRICT
	#endregion
#else     // MONO_STRICT

public partial class Main
{
	
	public Main(IDbConnection connection) : 
			base(connection, new DbLinq.Sqlite.SqliteVendor())
	{
		this.OnCreated();
	}
	
	public Main(IDbConnection connection, IVendor sqlDialect) : 
			base(connection, sqlDialect)
	{
		this.OnCreated();
	}
	
	public Main(IDbConnection connection, MappingSource mappingSource, IVendor sqlDialect) : 
			base(connection, mappingSource, sqlDialect)
	{
		this.OnCreated();
	}
}
#region End Not MONO_STRICT
	#endregion
#endif     // MONO_STRICT
#endregion

[Table(Name="main.tip")]
public partial class Tip : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
{
	
	private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
	
	private string _content;
	
	private int _flag;
	
	private int _id;
	
	private int _learned;
	
	private int _marked;
	
	private string _title;
	
	#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnContentChanged();
		
		partial void OnContentChanging(string value);
		
		partial void OnFlagChanged();
		
		partial void OnFlagChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnLearnedChanged();
		
		partial void OnLearnedChanging(int value);
		
		partial void OnMarkedChanged();
		
		partial void OnMarkedChanging(int value);
		
		partial void OnTitleChanged();
		
		partial void OnTitleChanging(string value);
		#endregion
	
	
	public Tip()
	{
		this.OnCreated();
	}
	
	[Column(Storage="_content", Name="content", DbType="TEXT(3000)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Content
	{
		get
		{
			return this._content;
		}
		set
		{
			if (((_content == value) 
						== false))
			{
				this.OnContentChanging(value);
				this.SendPropertyChanging();
				this._content = value;
				this.SendPropertyChanged("Content");
				this.OnContentChanged();
			}
		}
	}
	
	[Column(Storage="_flag", Name="flag", DbType="INTEGER", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Flag
	{
		get
		{
			return this._flag;
		}
		set
		{
			if ((_flag != value))
			{
				this.OnFlagChanging(value);
				this.SendPropertyChanging();
				this._flag = value;
				this.SendPropertyChanged("Flag");
				this.OnFlagChanged();
			}
		}
	}
	
	[Column(Storage="_id", Name="id", DbType="INTEGER", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int ID
	{
		get
		{
			return this._id;
		}
		set
		{
			if ((_id != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._id = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[Column(Storage="_learned", Name="learned", DbType="INTEGER", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Learned
	{
		get
		{
			return this._learned;
		}
		set
		{
			if ((_learned != value))
			{
				this.OnLearnedChanging(value);
				this.SendPropertyChanging();
				this._learned = value;
				this.SendPropertyChanged("Learned");
				this.OnLearnedChanged();
			}
		}
	}
	
	[Column(Storage="_marked", Name="marked", DbType="INTEGER", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public int Marked
	{
		get
		{
			return this._marked;
		}
		set
		{
			if ((_marked != value))
			{
				this.OnMarkedChanging(value);
				this.SendPropertyChanging();
				this._marked = value;
				this.SendPropertyChanged("Marked");
				this.OnMarkedChanged();
			}
		}
	}
	
	[Column(Storage="_title", Name="title", DbType="TEXT(100)", AutoSync=AutoSync.Never, CanBeNull=false)]
	[DebuggerNonUserCode()]
	public string Title
	{
		get
		{
			return this._title;
		}
		set
		{
			if (((_title == value) 
						== false))
			{
				this.OnTitleChanging(value);
				this.SendPropertyChanging();
				this._title = value;
				this.SendPropertyChanged("Title");
				this.OnTitleChanged();
			}
		}
	}
	
	public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
	
	public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
		if ((h != null))
		{
			h(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(string propertyName)
	{
		System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
		if ((h != null))
		{
			h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
		}
	}
}
