﻿using System;
using System.Configuration;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace iControlBaike
{
    public partial class MainForm : Form
    {
        private SQLiteConnection dbConnection;
        private Main dataContext;
        private bool exit;

        public MainForm()
        {
            InitializeComponent();

            this.Text = Properties.Resources.strAppName;
            this.Icon = Properties.Resources.iconApp;

            this.notifyIcon.Icon = Properties.Resources.iconApp;
            this.notifyIcon.Text = Properties.Resources.strAppName;
            this.menuNotifyIconVersion.Text = String.Format(Properties.Resources.strCurrentVersionFormat, this.getVersionString());

            this.dbConnection = new SQLiteConnection(@"Data Source=data.db;DbLinqProvider=Sqlite;");
            this.dataContext = new Main(this.dbConnection);
            this.updateCategory();
        }

        private void updateCategory()
        {
            IQueryable<Tip> items = this.dataContext.Tip;

            if (this.radNotLearned.Checked)
            {
                items = items.Where(i => i.Learned == 0);
            }
            else if (this.radMarked.Checked)
            {
                items = items.Where(i => i.Marked == 1);
            }

            string filterText = this.txtFilter.Text.Trim();
            if (!String.IsNullOrEmpty(filterText))
            {
                items = items.Where(i => i.Title.Contains(filterText));
            }

            this.lstTips.BeginUpdate();
            this.lstTips.Items.Clear();
            this.lstTips.Items.AddRange(items.ToArray());
            if (this.lstTips.Items.Count > 0)
                this.lstTips.SelectedIndex = 0;
            this.lstTips.EndUpdate();

            bool isItemsEmpty = (items.Count() == 0);
            this.txtContent.Visible = !isItemsEmpty;
            this.tableLayoutPanel3.Visible = !isItemsEmpty;
        }

        private void category_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == this.radAll)
            {
                this.radAll.BackgroundImage = Properties.Resources.imgCategorySelectedBk;
                this.radNotLearned.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
                this.radMarked.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
            }
            else if (sender == this.radMarked)
            {
                this.radMarked.BackgroundImage = Properties.Resources.imgCategorySelectedBk;
                this.radAll.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
                this.radNotLearned.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
            }
            else
            {
                this.radNotLearned.BackgroundImage = Properties.Resources.imgCategorySelectedBk;
                this.radAll.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
                this.radMarked.BackgroundImage = Properties.Resources.imgCategoryNotSelectedBk;
            }
            this.updateCategory();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            this.updateCategory();
        }

        private void lstTips_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = 28;
        }

        private void lstTips_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
                return;
            Tip tip = (Tip)lstTips.Items[e.Index];
            Color clrText;

            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(55, 68, 154)), e.Bounds);
                clrText = Color.White;
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.White, e.Bounds);
                clrText = Color.FromArgb(42, 42, 42);
            }

            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.FormatFlags = StringFormatFlags.NoWrap;
            sf.Trimming = StringTrimming.EllipsisCharacter;

            int rightSpace = 0;
            if (tip.Marked == 1)
            {
                e.Graphics.DrawImage(Properties.Resources.imgMarked, e.Bounds.Right - 20, (e.Bounds.Top + e.Bounds.Bottom) / 2 - 9, 18, 18);
                rightSpace += 20;
            }
            if (tip.Learned == 1)
            {
                e.Graphics.DrawImage(Properties.Resources.imgLearned, e.Bounds.Right - 20 - rightSpace, (e.Bounds.Top + e.Bounds.Bottom) / 2 - 9, 18, 18);
                rightSpace += 20;
            } 

            e.Graphics.DrawString(String.Format("Q{0}. {1}", tip.Content, tip.Title),
                this.Font, new SolidBrush(clrText), new Rectangle(e.Bounds.Left, e.Bounds.Top, e.Bounds.Width - rightSpace, e.Bounds.Height), sf);

            e.DrawFocusRectangle();
        }

        private string getTipPath(Tip tip)
        {
            return Path.Combine(Path.Combine(Application.StartupPath, "tips"), tip.Content + ".rtf");
        }

        private void updateContent()
        {
            Tip tip = (Tip)this.lstTips.SelectedItem;
            this.lblTitle.Text = String.Format("Q{0}. {1}", tip.Content, tip.Title);
            this.txtContent.LoadFile(this.getTipPath(tip));
            this.chkLearned.CheckedChanged -= this.chkLearned_CheckedChanged;
            this.chkMarked.CheckedChanged -= this.chkMarked_CheckedChanged;
            this.chkLearned.Checked = (tip.Learned == 1);
            this.chkLearned.BackgroundImage = this.chkLearned.Checked ? Properties.Resources.imgLearnedButtonBk : Properties.Resources.imgNotLearnedButtonBk;
            this.chkMarked.Checked = (tip.Marked == 1);
            this.chkMarked.BackgroundImage = this.chkMarked.Checked ? Properties.Resources.imgMarkedButtonBk : Properties.Resources.imgNotMarkedButtonBk;
            this.chkLearned.CheckedChanged += this.chkLearned_CheckedChanged;
            this.chkMarked.CheckedChanged += this.chkMarked_CheckedChanged;
            this.toolTip.SetToolTip(this.lblTitle, tip.Title);
        }

        private void lstTips_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.updateContent();
        }

        private void chkLearned_CheckedChanged(object sender, EventArgs e)
        {
            Tip tip = (Tip)this.lstTips.SelectedItem;
            if (this.chkLearned.Checked)
            {
                this.chkLearned.BackgroundImage = Properties.Resources.imgLearnedButtonBk;
                tip.Learned = 1;
            }
            else
            {
                this.chkLearned.BackgroundImage = Properties.Resources.imgNotLearnedButtonBk;
                tip.Learned = 0;
            }
            this.lstTips.Invalidate(this.lstTips.GetItemRectangle(this.lstTips.SelectedIndex));
            this.dataContext.SubmitChanges();
        }

        private void chkMarked_CheckedChanged(object sender, EventArgs e)
        {
            Tip tip = (Tip)this.lstTips.SelectedItem;
            if (this.chkMarked.Checked)
            {
                this.chkMarked.BackgroundImage = Properties.Resources.imgMarkedButtonBk;
                tip.Marked = 1;
            }
            else
            {
                this.chkMarked.BackgroundImage = Properties.Resources.imgNotMarkedButtonBk;
                tip.Marked = 0;
            }
            this.lstTips.Invalidate(this.lstTips.GetItemRectangle(this.lstTips.SelectedIndex));
            this.dataContext.SubmitChanges();
        }

        private void openWindow()
        {
            this.Show();
            this.ShowInTaskbar = true;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;
            this.Activate();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.openWindow();
        }

        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                this.openWindow();
        }

        private void menuNotifyIconOpen_Click(object sender, EventArgs e)
        {
            this.openWindow();
        }

        private void menuNotifyIconExit_Click(object sender, EventArgs e)
        {
            this.exit = true;
            this.Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.exit)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            if (this.notifyIcon.Tag != null)
            {
                this.txtFilter.Text = String.Empty;
                this.radAll.Checked = true;
                this.updateCategory();
                this.lstTips.SelectedItem = (Tip)this.notifyIcon.Tag;
            }
            this.openWindow();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var item = config.AppSettings.Settings["FirstTimeMinimized"];
                if (item == null || item.Value.Equals(true.ToString()))
                {
                    if (item == null)
                    {
                        config.AppSettings.Settings.Add("FirstTimeMinimized", false.ToString());
                    }
                    else
                    {
                        item.Value = false.ToString();
                    }
                    config.Save(ConfigurationSaveMode.Modified);
                    this.notifyIcon.Tag = null;
                    this.notifyIcon.ShowBalloonTip(30000, Properties.Resources.strFirstTimeMinimizedTitle, Properties.Resources.strFirstTimeMinimizedText, ToolTipIcon.Info);
                }
            }
        }

        private string getVersionString(bool full = true)
        {
            String version = Application.ProductVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                version = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            if (!full)
            {
                string[] segments = version.Split('.');
                version = String.Format("{0}.{1}", segments[0], segments[1]);
            }
            return version;
        }

        private string getTipContentText(Tip tip)
        {
            using (RichTextBox rtb = new RichTextBox())
            {
                rtb.LoadFile(getTipPath(tip));
                return rtb.Text;
            }
        }

        private void showNextTipBalloon()
        {
            if (this.WindowState != FormWindowState.Minimized)
                return;
            if (this.dataContext.Tip.FirstOrDefault(i => i.Learned == 0) == null)
                return;
            Tip nextTip = this.dataContext.Tip.FirstOrDefault(i => i.Learned == 0 && i.Flag == 0);
            if (nextTip == null)
            {
                var items = from i in dataContext.Tip where i.Learned == 0 && i.Flag == 1 select i;
                foreach (var item in items)
                {
                    item.Flag = 0;
                }
                this.dataContext.SubmitChanges();
            }
            nextTip = this.dataContext.Tip.FirstOrDefault(i => i.Learned == 0 && i.Flag == 0);
            nextTip.Flag = 1;
            this.dataContext.SubmitChanges();
            this.notifyIcon.Tag = nextTip;
            this.notifyIcon.ShowBalloonTip(60000, nextTip.Title, this.getTipContentText(nextTip), ToolTipIcon.Info);
        }

        private void timerShowTip_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = DateTime.Now.TimeOfDay;

            if (ts.Hours == 10 && ts.Minutes == 0)
            {
                this.showNextTipBalloon();
            }
            else if (ts.Hours == 17 && ts.Minutes == 0)
            {
                this.showNextTipBalloon();
            }
        }
    }
}
