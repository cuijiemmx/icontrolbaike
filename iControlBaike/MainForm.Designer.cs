﻿namespace iControlBaike
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuNotifyIcon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuNotifyIconVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotifyIconOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotifyIconExit = new System.Windows.Forms.ToolStripMenuItem();
            this.timerShowTip = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radAll = new System.Windows.Forms.RadioButton();
            this.radNotLearned = new System.Windows.Forms.RadioButton();
            this.radMarked = new System.Windows.Forms.RadioButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lstTips = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtFilter = new wmgCMS.WaterMarkTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.chkMarked = new System.Windows.Forms.CheckBox();
            this.chkLearned = new System.Windows.Forms.CheckBox();
            this.panelSeperator = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuNotifyIcon.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImage = global::iControlBaike.Properties.Resources.imgSearch;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(274, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox1, "输入知识标题快速搜索");
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenuStrip = this.menuNotifyIcon;
            this.notifyIcon.Visible = true;
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.notifyIcon_BalloonTipClicked);
            this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseClick);
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // menuNotifyIcon
            // 
            this.menuNotifyIcon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNotifyIconVersion,
            this.menuNotifyIconOpen,
            this.menuNotifyIconExit});
            this.menuNotifyIcon.Name = "menuNotifyIcon";
            this.menuNotifyIcon.Size = new System.Drawing.Size(153, 70);
            // 
            // menuNotifyIconVersion
            // 
            this.menuNotifyIconVersion.Enabled = false;
            this.menuNotifyIconVersion.Name = "menuNotifyIconVersion";
            this.menuNotifyIconVersion.Size = new System.Drawing.Size(152, 22);
            this.menuNotifyIconVersion.Text = "版本";
            // 
            // menuNotifyIconOpen
            // 
            this.menuNotifyIconOpen.Name = "menuNotifyIconOpen";
            this.menuNotifyIconOpen.Size = new System.Drawing.Size(152, 22);
            this.menuNotifyIconOpen.Text = "打开i·掌控百科";
            this.menuNotifyIconOpen.Click += new System.EventHandler(this.menuNotifyIconOpen_Click);
            // 
            // menuNotifyIconExit
            // 
            this.menuNotifyIconExit.Name = "menuNotifyIconExit";
            this.menuNotifyIconExit.Size = new System.Drawing.Size(152, 22);
            this.menuNotifyIconExit.Text = "退出";
            this.menuNotifyIconExit.Click += new System.EventHandler(this.menuNotifyIconExit_Click);
            // 
            // timerShowTip
            // 
            this.timerShowTip.Enabled = true;
            this.timerShowTip.Interval = 60000;
            this.timerShowTip.Tick += new System.EventHandler(this.timerShowTip_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(898, 549);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(68)))), ((int)(((byte)(154)))));
            this.flowLayoutPanel1.Controls.Add(this.radAll);
            this.flowLayoutPanel1.Controls.Add(this.radNotLearned);
            this.flowLayoutPanel1.Controls.Add(this.radMarked);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(898, 38);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // radAll
            // 
            this.radAll.Appearance = System.Windows.Forms.Appearance.Button;
            this.radAll.BackgroundImage = global::iControlBaike.Properties.Resources.imgCategorySelectedBk;
            this.radAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radAll.Checked = true;
            this.radAll.FlatAppearance.BorderSize = 0;
            this.radAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radAll.Font = new System.Drawing.Font("Microsoft YaHei", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radAll.ForeColor = System.Drawing.Color.White;
            this.radAll.Location = new System.Drawing.Point(7, 7);
            this.radAll.Name = "radAll";
            this.radAll.Size = new System.Drawing.Size(54, 24);
            this.radAll.TabIndex = 0;
            this.radAll.TabStop = true;
            this.radAll.Text = "全部";
            this.radAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radAll.UseVisualStyleBackColor = true;
            this.radAll.CheckedChanged += new System.EventHandler(this.category_CheckedChanged);
            // 
            // radNotLearned
            // 
            this.radNotLearned.Appearance = System.Windows.Forms.Appearance.Button;
            this.radNotLearned.BackgroundImage = global::iControlBaike.Properties.Resources.imgCategoryNotSelectedBk;
            this.radNotLearned.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radNotLearned.FlatAppearance.BorderSize = 0;
            this.radNotLearned.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radNotLearned.Font = new System.Drawing.Font("Microsoft YaHei", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radNotLearned.ForeColor = System.Drawing.Color.White;
            this.radNotLearned.Location = new System.Drawing.Point(67, 7);
            this.radNotLearned.Name = "radNotLearned";
            this.radNotLearned.Size = new System.Drawing.Size(54, 24);
            this.radNotLearned.TabIndex = 1;
            this.radNotLearned.Text = "未掌握";
            this.radNotLearned.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radNotLearned.UseVisualStyleBackColor = true;
            this.radNotLearned.CheckedChanged += new System.EventHandler(this.category_CheckedChanged);
            // 
            // radMarked
            // 
            this.radMarked.Appearance = System.Windows.Forms.Appearance.Button;
            this.radMarked.BackgroundImage = global::iControlBaike.Properties.Resources.imgCategoryNotSelectedBk;
            this.radMarked.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radMarked.FlatAppearance.BorderSize = 0;
            this.radMarked.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radMarked.Font = new System.Drawing.Font("Microsoft YaHei", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radMarked.ForeColor = System.Drawing.Color.White;
            this.radMarked.Location = new System.Drawing.Point(127, 7);
            this.radMarked.Name = "radMarked";
            this.radMarked.Size = new System.Drawing.Size(54, 24);
            this.radMarked.TabIndex = 2;
            this.radMarked.Text = "已标记";
            this.radMarked.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radMarked.UseVisualStyleBackColor = true;
            this.radMarked.CheckedChanged += new System.EventHandler(this.category_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 41);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(892, 505);
            this.splitContainer1.SplitterDistance = 295;
            this.splitContainer1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lstTips, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(295, 505);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lstTips
            // 
            this.lstTips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstTips.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.lstTips.ForeColor = System.Drawing.Color.Red;
            this.lstTips.FormattingEnabled = true;
            this.lstTips.IntegralHeight = false;
            this.lstTips.Location = new System.Drawing.Point(3, 31);
            this.lstTips.Name = "lstTips";
            this.lstTips.Size = new System.Drawing.Size(289, 471);
            this.lstTips.TabIndex = 0;
            this.lstTips.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstTips_DrawItem);
            this.lstTips.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.lstTips_MeasureItem);
            this.lstTips.SelectedIndexChanged += new System.EventHandler(this.lstTips_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.txtFilter);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(295, 28);
            this.panel3.TabIndex = 3;
            // 
            // txtFilter
            // 
            this.txtFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilter.Font = new System.Drawing.Font("Microsoft YaHei", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilter.Location = new System.Drawing.Point(3, 3);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(289, 22);
            this.txtFilter.TabIndex = 1;
            this.txtFilter.WaterMarkColor = System.Drawing.Color.DarkGray;
            this.txtFilter.WaterMarkText = "输入知识标题快速搜索...";
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 505);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::iControlBaike.Properties.Resources.imgNoItemSelected;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.txtContent);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(593, 465);
            this.panel2.TabIndex = 3;
            // 
            // txtContent
            // 
            this.txtContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(228)))), ((int)(((byte)(231)))));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContent.Location = new System.Drawing.Point(0, 0);
            this.txtContent.Name = "txtContent";
            this.txtContent.ReadOnly = true;
            this.txtContent.Size = new System.Drawing.Size(593, 465);
            this.txtContent.TabIndex = 3;
            this.txtContent.Text = "";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.chkMarked, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.chkLearned, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panelSeperator, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(593, 40);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoEllipsis = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblTitle.Location = new System.Drawing.Point(3, 10);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(521, 23);
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "label1";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkMarked
            // 
            this.chkMarked.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkMarked.BackgroundImage = global::iControlBaike.Properties.Resources.imgNotMarkedButtonBk;
            this.chkMarked.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkMarked.FlatAppearance.BorderSize = 0;
            this.chkMarked.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.chkMarked.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.chkMarked.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.chkMarked.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkMarked.Location = new System.Drawing.Point(561, 1);
            this.chkMarked.Margin = new System.Windows.Forms.Padding(1);
            this.chkMarked.Name = "chkMarked";
            this.chkMarked.Size = new System.Drawing.Size(31, 31);
            this.chkMarked.TabIndex = 1;
            this.chkMarked.UseVisualStyleBackColor = true;
            this.chkMarked.CheckedChanged += new System.EventHandler(this.chkMarked_CheckedChanged);
            // 
            // chkLearned
            // 
            this.chkLearned.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkLearned.BackgroundImage = global::iControlBaike.Properties.Resources.imgNotLearnedButtonBk;
            this.chkLearned.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkLearned.FlatAppearance.BorderSize = 0;
            this.chkLearned.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.chkLearned.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.chkLearned.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.chkLearned.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkLearned.Location = new System.Drawing.Point(528, 1);
            this.chkLearned.Margin = new System.Windows.Forms.Padding(1);
            this.chkLearned.Name = "chkLearned";
            this.chkLearned.Size = new System.Drawing.Size(31, 31);
            this.chkLearned.TabIndex = 2;
            this.chkLearned.UseVisualStyleBackColor = true;
            this.chkLearned.CheckedChanged += new System.EventHandler(this.chkLearned_CheckedChanged);
            // 
            // panelSeperator
            // 
            this.panelSeperator.BackgroundImage = global::iControlBaike.Properties.Resources.imgHorizontalSeperator;
            this.panelSeperator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel3.SetColumnSpan(this.panelSeperator, 3);
            this.panelSeperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSeperator.Location = new System.Drawing.Point(0, 36);
            this.panelSeperator.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panelSeperator.Name = "panelSeperator";
            this.panelSeperator.Size = new System.Drawing.Size(593, 1);
            this.panelSeperator.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(228)))), ((int)(((byte)(231)))));
            this.ClientSize = new System.Drawing.Size(898, 549);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuNotifyIcon.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip menuNotifyIcon;
        private System.Windows.Forms.ToolStripMenuItem menuNotifyIconOpen;
        private System.Windows.Forms.ToolStripMenuItem menuNotifyIconExit;
        private System.Windows.Forms.Timer timerShowTip;
        private System.Windows.Forms.ToolStripMenuItem menuNotifyIconVersion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton radAll;
        private System.Windows.Forms.RadioButton radNotLearned;
        private System.Windows.Forms.RadioButton radMarked;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ListBox lstTips;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private wmgCMS.WaterMarkTextBox txtFilter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox txtContent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.CheckBox chkMarked;
        private System.Windows.Forms.CheckBox chkLearned;
        private System.Windows.Forms.Panel panelSeperator;
    }
}

