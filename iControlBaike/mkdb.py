#! /usr/bin/env python

import xlrd
import os
import sqlite3

def script_dir():
    return os.path.dirname(__file__)

def main():
    dbpath = os.path.join(script_dir(), 'data.db')
    db = sqlite3.connect(dbpath)
    c = db.cursor()
    c.execute('drop table if exists tip')
    c.execute('create table tip(id INTEGER PRIMARY KEY, title TEXT(100), content TEXT(10), marked INTEGER, learned INTEGER, flag INTEGER)')
    tips_xls = os.path.join(script_dir(), 'tips.xls')
    wb = xlrd.open_workbook(tips_xls)
    sh = wb.sheet_by_index(0)
    for i in xrange(sh.nrows):
        title = sh.cell_value(i, 0)
        content = sh.cell_value(i, 1)
        c.execute('insert into tip values(?, ?, ?, ?, ?, ?)', (i, title, content, 0, 0, 0))
    db.commit()
    db.close()

if __name__ == '__main__':
    main()